using demo1;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers()
    .AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
    });
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options => {
    //注释
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    //第二个参数为是否显示控制器注释,我们选择true
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename), true);

    //生成多个文档显示
    typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
    {
        //添加文档介绍
        options.SwaggerDoc(version, new OpenApiInfo
        {
            Title = $"项目名",
            Version = version,
            Description = $"项目名:{version}版本"
        });
    });
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
}

app.UseSwagger();
app.UseSwaggerUI(options => {

    /*
     options.SwaggerEndpoint($"/swagger/V1/swagger.json",$"版本选择:V1");
    */
    //如果只有一个版本也要和上方保持一致
    typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
    {
        //切换版本操作
        //参数一是使用的哪个json文件,参数二就是个名字
        options.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"版本选择:{version}");
    });
});

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
