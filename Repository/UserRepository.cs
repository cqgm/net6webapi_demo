﻿using demo;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class UserRepository
    {
        public UserRepository(ISqlSugarClient db)
        {
            Db = db;
        }

        public ISqlSugarClient Db { get; }

        public List<User> GetList()
        {
            return Db.Queryable<User>().ToList();
        }
    }
}
