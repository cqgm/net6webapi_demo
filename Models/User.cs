﻿using SqlSugar;
namespace demo
{
    [SugarTable("T_User")]
    /// <summary>
    /// 测试-用户类
    /// </summary>
    public class User
    {
        [SugarColumn(IsIdentity = true, IsPrimaryKey = true)]
        /// <summary>
        /// 用户id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 用户名称
        /// </summary>
        [SugarColumn(ColumnName = "Name")]
        public string UserName { get; set; }
        /// <summary>
        /// 年龄
        /// </summary>
        public int Age { get; set; }
    }
}
