﻿using AutoMapper;
using demo.TestClass;

namespace demo.Profiles
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<User,UserDto>();
        }      
    }
}
