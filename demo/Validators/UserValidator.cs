﻿using FluentValidation;

namespace demo.Validators
{
    public class UserValidator: AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(it => it.UserName)
            .NotNull().WithMessage("用户名不能为空")
            .Must(v => v.Contains(" ")).WithMessage("用户名必须包含空格");

            RuleFor(it => it.Age)
            .NotNull().WithMessage("年龄不能为空");
            //.Must(v => v.Contains(" ")).WithMessage("编号不能包含空格");
        }
    }
}
