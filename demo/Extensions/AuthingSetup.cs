﻿using Authing.ApiClient.Auth;
using Authing.ApiClient.Auth.Types;

namespace demo.Extensions
{
    public static class AuthingSetup
    {
        public static void AddAuthingSetup(this IServiceCollection services, IConfiguration configuration)
        {
            var authenticationClient = new AuthenticationClient(options =>
            {
                options.AppId = configuration["Authing.Config:AppId"];
                options.Host = configuration["Authing.Config:AppHost"];
                options.Secret = configuration["Authing.Config:Secret"];
                options.RedirectUri = configuration["Authing.Config:RedirectUri"];
                options.TokenEndPointAuthMethod = TokenEndPointAuthMethod.CLIENT_SECRET_POST;
                options.Protocol = Protocol.OIDC;
            });
            // 将 authenticationClient 注册为单例，并加入容器中去
            services.AddSingleton(typeof(AuthenticationClient), authenticationClient);
        }
    }
}
