using Common;
using demo;
using demo.Extensions;
using demo.Profiles;
using FluentValidation.AspNetCore;
using IServices;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Repository;
using Services;
using System.Reflection;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers()
        .AddNewtonsoftJson(options =>
        {
            options.SerializerSettings.DateFormatString = "yyyy-MM-dd HH:mm:ss";
        });
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
builder.Services.AddSwaggerGen(options => {
    //注释
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    //第二个参数为是否显示控制器注释,我们选择true
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename), true);

    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Description = "在下框中输入请求头中需要添加Jwt授权Token：Bearer Token",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] { }
        }
    });

    /*
    //只有参数里的V1是要和下面配置路径保持一致,
    //剩下的乱写也不报错 但是不推荐
     options.SwaggerDoc("V1", new OpenApiInfo 
        {
            Title = $"项目名",
            Version = "V1",
            Description = $"项目名:V1版本"
        });
    */
    //生成多个文档显示
    typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
    {
        //添加文档介绍
        options.SwaggerDoc(version, new OpenApiInfo
        {
            Title = $"项目名",
            Version = version,
            Description = $"项目名:{version}版本"
        });
    });
});
builder.Services.AddSqlsugarSetup(builder.Configuration);
//builder.Services.AddScoped<UserRepository>();
builder.Services.AddScoped(typeof(Repository<>));
builder.Services.AddScoped<IUserService,UserService>();
builder.Services.AddFluentValidation(opt => {
    opt.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
});
#region   配置跨域
builder.Services.AddCors(c =>
{
    c.AddPolicy("Cors", policy =>
    {
        policy
              .AllowAnyOrigin()
              .AllowAnyHeader()//Ensures that the policy allows any header.
              .AllowAnyMethod();
    });
});
#endregion
#region Jwt
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        var tokenModel = builder.Configuration.GetSection("Jwt").Get<TokenModelJwt>();
        var secretByte = Encoding.UTF8.GetBytes(tokenModel.Secret);
        options.TokenValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidIssuer = tokenModel.Issuer,

            ValidateAudience = true,
            ValidAudience = tokenModel.Audience,

            ValidateLifetime = true,

            IssuerSigningKey = new SymmetricSecurityKey(secretByte)
        };
        options.Events = new JwtBearerEvents
        {
            OnChallenge = context =>
            {
                //此处代码为终止.Net Core默认的返回类型和数据结果，这个很重要哦，必须
                //context.HandleResponse();

                //自定义自己想要返回的数据结果，我这里要返回的是Json对象，通过引用Newtonsoft.Json库进行转换

                //自定义返回的数据类型
                //context.Response.ContentType = "text/plain";
                ////自定义返回状态码，默认为401 我这里改成 200
                ////context.Response.StatusCode = StatusCodes.Status200OK;
                //context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                ////输出Json数据结果
                //context.Response.WriteAsync("expired");
                return Task.FromResult(0);
            },
            //403
            OnForbidden = context =>
            {
                //context.Response.ContentType = "text/plain";
                ////自定义返回状态码，默认为401 我这里改成 200
                ////context.Response.StatusCode = StatusCodes.Status200OK;
                //context.Response.StatusCode = StatusCodes.Status401Unauthorized;
                ////输出Json数据结果
                //context.Response.WriteAsync("expired");
                return Task.FromResult(0);
            }

        };
    });
#endregion
//注入缓存
builder.Services.AddMemoryCache();
builder.Services.AddAutoMapper(typeof(MapperProfile));
builder.Services.AddAuthingSetup(builder.Configuration);
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSession(options =>
{
    options.Cookie.Name = "This.Session";
    options.IdleTimeout = TimeSpan.FromSeconds(2000);//设置session的过期时间
    options.Cookie.HttpOnly = true;//设置在浏览器不能通过js获得该cookie的值
    options.Cookie.IsEssential = true;
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{

}

app.UseSwagger();
//app.UseSwaggerUI();
app.UseSwaggerUI(options => {

    /*
     options.SwaggerEndpoint($"/swagger/V1/swagger.json",$"版本选择:V1");
    */
    //如果只有一个版本也要和上方保持一致
    typeof(ApiVersions).GetEnumNames().ToList().ForEach(version =>
    {
        //切换版本操作
        //参数一是使用的哪个json文件,参数二就是个名字
        options.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"版本选择:{version}");
    });
});

app.UseCors("Cors");
app.UseHttpsRedirection();
app.UseSession();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
