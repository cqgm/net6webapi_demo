﻿using AutoMapper;
using demo.TestClass;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapperController : ControllerBase
    {
        public MapperController(IMapper mapper)
        {
            Mapper = mapper;
        }

        public IMapper Mapper { get; }

        [HttpPost]
        public UserDto Test(User user)
        {
            return Mapper.Map<User, UserDto>(user);
        }
    }
}
