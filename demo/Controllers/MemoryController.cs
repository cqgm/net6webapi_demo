﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemoryController : ControllerBase
    {
        private readonly IMemoryCache _cache;
        //还是通过构造函数的方法，获取
        public MemoryController(IMemoryCache cache)
        {
            _cache = cache;
        }

        [HttpPost]
        public string Set(string Name)
        {
            var key = Guid.NewGuid().ToString();
            _cache.Set(key, Name, TimeSpan.FromSeconds(100));
            return key;
        }

        /// <summary>
        /// 获取缓存key值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpGet]
        public string Get(string key)
        {
            //?前不为空执行?后语句
            return _cache.Get(key)?.ToString();
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost("deletekey")]
        public string Delete(string key)
        {
            _cache.Remove(key);
            return _cache.Get(key)?.ToString();
        }

    }
}
