﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        [HttpPost]
        public bool Upload(IFormFile file)
        {
            //同名上传会替换
            var fileName = file.FileName;
            var path = "D:/Test/";
            //使用using 可以自动释放这个文件流
            using (var stream = new FileStream(path + fileName, FileMode.Create))
            {
                file.CopyTo(stream);
            }
            return true;
        }
    }
}
