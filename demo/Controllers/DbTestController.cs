﻿using IServices;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using SqlSugar;

namespace demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DbTestController : ControllerBase
    {
        public DbTestController(ISqlSugarClient db,IUserService userService)
        {
            Db = db;
            UserService = userService;
        }

        private ISqlSugarClient Db { get; }
        private IUserService UserService { get; }

        /// <summary>
        /// 创建数据库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public bool CreatDb()
        {
            return Db.DbMaintenance.CreateDatabase();
        }

        /// <summary>
        /// 创建用户表
        /// </summary>
        [HttpPost("table")]
        public void CreateTable()
        {
            Db.CodeFirst.InitTables(typeof(User));
        }
        /// <summary>
        /// 查询所有用户
        /// </summary>
        /// <typeparam name="User"></typeparam>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        [HttpGet("users")]
        public List<User> GetList()
        {
            return UserService.GetUsers();
        }
        /// <summary>
        /// 数据验证
        /// </summary>
        /// <returns></returns>
        [HttpPost("users")]
        public List<User> GetListValidatortest([FromBody] User user)
        {
            return UserService.GetUsers();
        }

        [HttpGet("sqlUser")]
        public List<User> GetSqlList()
        {
            var dt = Db.Ado.SqlQuery<User>("select * from T_User");
            return dt;
        }

        /// <summary>
        /// 生成实体类
        /// </summary>
        [HttpPost("genClass_demo1")]
        public void GenClass_demo1()
        {
            var table_name = "CYYJSensorIndex";
            var table_name1 = "CYYJSensorRealDataList";

            var dt = Db.Ado.GetDataTable("select * from "+ table_name);
            var dt1 = Db.Ado.GetDataTable("select * from " + table_name1);

            //Db.DbFirst.Where("Student").CreateClassFile("D:\\醉经\\api_test_77\\Models", "Models");
        }
    }
}
