﻿using Common;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;

namespace demo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JwtController : ControllerBase
    {
        public JwtController(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        [HttpPost("jwt")]
        public string CreateToken()
        {
            var tokenModel = Configuration.GetSection("Jwt").Get<TokenModelJwt>();
            tokenModel.UserName = "张三";
            tokenModel.UserId = 1;
            tokenModel.Role = "admin";
            return Common.JwtHelper.CreateJwt(tokenModel);
        }

        [HttpGet]
        [Authorize]
        public IActionResult DeToken([FromHeader] string Authorization)
        {
            var token = JwtHelper.SerializeJwt(Authorization.Replace("Bearer ", ""));
            return Ok(token);
        }

        [HttpPost("jwtdto")]
        public string CreateTokendto(LoginDto loginDto)
        {
            var tokenModel = Configuration.GetSection("Jwt").Get<TokenModelJwt>();
            tokenModel.UserName = "张三";
            tokenModel.UserId = 1;
            tokenModel.Role = "admin";
            return Common.JwtHelper.CreateJwt(tokenModel);
        }
    }
}
