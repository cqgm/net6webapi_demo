﻿using demo;
using IServices;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class UserService:IUserService
    {
        //public UserService(UserRepository userRepository)
        //{
        //    UserRepository = userRepository;
        //}
        public UserService(Repository<User> userRepository)
        {
            UserRepository = userRepository;
        }

        public Repository<User> UserRepository { get; }

        public List<User> GetUsers()
        {
            return UserRepository.GetList();
        }
    }
}
